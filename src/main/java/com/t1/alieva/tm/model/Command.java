package com.t1.alieva.tm.model;


import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;

public class Command {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show exitdeveloper info."
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application"
    );
    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show information about device"
    );

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public static Command getABOUT() {
        return ABOUT;
    }

    public static void setABOUT(Command ABOUT) {
        Command.ABOUT = ABOUT;
    }

    public static Command getVERSION() {
        return VERSION;
    }

    public static void setVERSION(Command VERSION) {
        Command.VERSION = VERSION;
    }

    public static Command getHELP() {
        return HELP;
    }

    public static void setHELP(Command HELP) {
        Command.HELP = HELP;
    }

    public static Command getEXIT() {
        return EXIT;
    }

    public static void setEXIT(Command EXIT) {
        Command.EXIT = EXIT;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Command getINFO() {
        return INFO;
    }

    public static void setINFO(Command INFO) {
        Command.INFO = INFO;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }
}


