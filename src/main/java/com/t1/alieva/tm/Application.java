package com.t1.alieva.tm;

import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.model.Command;
import com.t1.alieva.tm.util.FormatUtil;

import java.util.Scanner;

import static com.t1.alieva.tm.constant.TerminalConst.*;

public final class Application {
    public static void main(String[] args) {

        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("**WELCOME to TASK-MANAGER**");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showErrorArgument() {
        System.err.println("Error! This argument is not supported...  ");
    }

    public static void showErrorCommand() {
        System.err.println("Error! This command is not supported...  ");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory (bytes): " + usageMemoryFormat);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:Alieva Djamilya");
        System.out.println("E-mail:jzama88@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.EXIT);
        System.out.println(Command.INFO);
    }
}
